<?php

namespace Tests\Unit;

use App\Models\Portfolio;
use PHPUnit\Framework\TestCase;

class PortfolioTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_that_portfolio_can_have_a_title()
    {
        $portfolio = Portfolio::factory()->create(["title" => "Portfolio Title"]);

        $this->assertEquals("Portfolio Title",$portfolio->title);
    }
}
