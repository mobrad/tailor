<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/portfolio', function (Request $request) {
//    return $request->user();
//});
Route::group(['prefix' => 'portfolio'],function(){
    Route::middleware('auth:sanctum')->group(function(){
         Route::post("/","PortfolioController@store");
         Route::post("/contents/{id}","PortfolioController@uploadContents");
    });
});
