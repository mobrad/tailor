<?php
namespace Modules\Portfolio\Services;

use App\Services\BaseService;
use Modules\Portfolio\Entities\Portfolio;

class PortfolioService extends BaseService
{
    public function __construct()
    {
        parent::__construct(Portfolio::class);
    }
    public function storeOrUpdate(array $attributes, int $id = null)
    {
        $attributes["user_id"] = auth()->user()->id;
        $model = $id == null ? new $this->class :  $this->find($id);
        unset($attributes['api_token']);
        $model->fill($attributes);
        $model->save();
        return $model;
    }

    public function addContent(array $attributes , int $portfolioId)
    {
        $portfolio = $this->find($portfolioId);
        $media = null;
        if($attributes['width'] && $attributes['height']){
        $media = $portfolio->addMedia(request()->file('media'))
            ->preservingOriginal()
            ->toMediaCollection('portfolios');
        }
        $media = $portfolio->addMedia(request()->file('media'))
            ->preservingOriginal()
            ->toMediaCollection('portfolios');
        return $media;

    }

}
