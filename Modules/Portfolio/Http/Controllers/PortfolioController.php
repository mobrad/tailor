<?php

namespace Modules\Portfolio\Http\Controllers;

use App\Http\Controllers\Controller;


use Modules\Portfolio\Http\Requests\PortfolioContentRequest;
use Modules\Portfolio\Services\PortfolioService;
use Modules\Portfolio\Http\Requests\PortfolioRequest;

class PortfolioController extends Controller
{
    public $portfolioService;
    public function __construct(PortfolioService $portfolioService)
    {
        $this->portfolioService = $portfolioService;
    }

    public function store(PortfolioRequest $request)
    {
        $data = $this->portfolioService->storeOrUpdate($request->validated());
        return  $this->sendResponse($data,"Portfolio Created");
    }
    public function uploadContents(PortfolioContentRequest $request,$id)
    {
        $data = $this->portfolioService->addContent($request->all(),$id);
        return $this->sendResponse($data,"Portfolio content added");
    }
}
