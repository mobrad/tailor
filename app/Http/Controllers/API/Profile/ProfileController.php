<?php

namespace App\Http\Controllers\API\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class ProfileController extends Controller
{
    //
    public function update(Request $request, UpdatesUserProfileInformation $updater)
    {
        $updater->update($request->user(), $request->all());
        return $this->sendResponse(
            $request->user()->fresh(),
            "User updated successfully"
        );
    }

    public function show(Request $request)
    {
        $user = $request->user();
        return $this->sendResponse($user , "User Profile Loaded successfully");
    }
}
