<?php


namespace App\Services;

use App\Models\User;
use OTPHP\TOTP;


class OTPService
{
   public function sendOtp(User $user)
   {
        $secret =$this->getUserSecret($user);

        // we generate otp with that secret
        $code = $this->generateOTP($secret);


        $user->sendOtpCodeforPasswordRecovery($code);


   }
   protected function getUserSecret($user)
   {
        // if user has a secret
        if($user->secret) {
            return $user->secret;
        }

        // user doesn't we create one for the user
        $otp = TOTP::create();
        $secret = $otp->getSecret();

        $user->update(['secret'=> $secret]);

        return $secret;
    }

    protected function generateOTP($secret)
    {
        $timestamp = time();

        $otp = TOTP::create($secret,1000);

        return $otp->at($timestamp);
    }

    public function verifyOTP($user, $code)
    {
        $secret = $this->getUserSecret($user);


       $timestamp = time();


       $otp = TOTP::create($secret,1000);


       $res = $otp->verify($code, $timestamp);

       return $res;
    }
}
