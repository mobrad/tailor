<?php


namespace App\Traits;


use App\Notifications\ResetPasswordOTPNotification;

trait OtpCode
{
    public function sendOtpCodeforPasswordRecovery($code)
    {
        $this->notify(new ResetPasswordOTPNotification($code));
    }
}
